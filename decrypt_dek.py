from Crypto.Cipher import AES
import duckdb
import gcsfs
import json
from google.auth import identity_pool
from google.cloud import kms
import os
import base64

# client1 credentials
client1_service_account = "svc-mta-attribution-cl1-cl2@tee-mta-dc-fdev-1c29.iam.gserviceaccount.com"
client1_workload_pool = "mta-attribution-cl1-cl2-pool"
client1_workload_provider = "mta-attribution-cl1-cl2-verifier"
client1_project_number = "964869236808"
client1_config_object = "gs://mta-cl1-cl2-config/attribution/config.json"
asymmetric_client1_key = "projects/tee-mta-dc-fdev-1c29/locations/us/keyRings/checking-pipe-ring-mta/cryptoKeys/checking-pipe-key-mta"

credential_template = '''{{
                           "type": "external_account",
                           "audience": "//iam.googleapis.com/projects/{project_number}/locations/global/workloadIdentityPools/{workload_pool}/providers/{workload_provider}",
                           "subject_token_type": "urn:ietf:params:oauth:token-type:jwt",
                           "token_url": "https://sts.googleapis.com/v1/token",
                           "credential_source": {{
                               "file": "/run/container_launcher/attestation_verifier_claims_token"
                           }},
                           "service_account_impersonation_url": "https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/{service_account}:generateAccessToken"
                       }}'''

cred_client1_json = credential_template.format(project_number=client1_project_number,
                                               workload_pool=client1_workload_pool,
                                               workload_provider=client1_workload_provider,
                                               service_account=client1_service_account)


client1_credentials = identity_pool.Credentials.from_info(json.loads(cred_client1_json)).with_scopes(['https://www.googleapis.com/auth/cloud-platform'])

fs_client1 = gcsfs.GCSFileSystem(token=client1_credentials)


print("READING CONFIG FILE FOR client1")
# READING CONFIG FILE FOR client1
with fs_client1.open(client1_config_object) as client1_json:
    config_client1 = json.loads(client1_json.read())

print(f"client1 config read from bucket {str(config_client1)}")

# this emulate MTA
input_bucket_client1 = config_client1['input_bucket']
dek_file = config_client1['dek_file']


def decrypt_asymmetric(key: str, ciphertext: bytes, credentials) -> kms.DecryptResponse:
    print("Start decrypting")
    client = kms.KeyManagementServiceClient(credentials=credentials)

    print("Call api")
    decrypt_response = client.asymmetric_decrypt(
        request={
            "name": key,
            "ciphertext": ciphertext,
        }
    )

    print(f"Response: {decrypt_response.plaintext!r}")
    return decrypt_response.plaintext

# Extract DEK
with fs_client1.open(f"{input_bucket_client1}/{dek_file}") as dek_json:
    dek = json.loads(decrypt_asymmetric(asymmetric_client1_key, dek_json.read(), client1_credentials))
    #dek = json.loads(dek_json.read())


def copy_to_local_disk(data_file, file_name):
    with open(file_name, "w") as local_file:
        local_file.write(data_file.read())

def decrypt(encrypted, key, iv):
    cipher = AES.new(key, AES.MODE_CBC, iv)
    decryptedtext = cipher.decrypt(base64.b64decode(encrypted))
    decryptedtextP = decryptedtext.rstrip(b'\0').decode("UTF-8")
    return decryptedtextP

def decrypt_local_file(file_name, dek):
    with open(file_name, "r") as local_file:
        return decrypt(local_file.read(), dek["key"], dek["iv"])


con = duckdb.connect(database=":memory:")

list_of_files = fs_client1.ls(input_bucket_client1)

def decrypt_delete_files(list_of_files, con):
    for index, file_name in enumerate(list_of_files):
        if file_name != dek_file:
            with fs_client1.open(f"{input_bucket_client1}/{file_name}") as data_file:
                copy_to_local_disk(data_file, file_name)
                decrypt_local_file(dek, file_name)
                if index == 0:
                    con.sql(f"CREATE TABLE campaing_fc AS (select * from read_parquet('{file_name}'));")
                else:
                    con.sql(f"INSERT INTO campaing_fc (select * from read_parquet('{file_name}'));")
                os.remove(file_name)

decrypt_delete_files(list_of_files, con)




con.sql("select count(1) from campaing_fc")


# def decrypt_delete_files(list_of_files):
#     decrypted_dataset = []
#     for file_name in list_of_files:
#         if file_name != dek_file:
#             with fs_client1.open(f"{input_bucket_client1}/{file_name}") as data_file:
#                 copy_to_local_disk(data_file, file_name)
#                 decrypt_local_file(dek, file_name)
#                 decrypted_dataset.append(file_name)
#     return decrypted_dataset

# decrypted_dataset = decrypt_delete_files(list_of_files)
# con.sql(f"select count(1) from read_parquet({decrypted_dataset}))")


con.close()
