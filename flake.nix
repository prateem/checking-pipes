{
  description = "Build image";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }:
  let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in {

    packages.x86_64-linux.default = pkgs.dockerTools.buildImage {
      name = "tee_docker_registry";
      tag = "tee_docker_tag";
      copyToRoot = pkgs.buildEnv {
        name = "image-root";
        paths = [
          pkgs.dockerTools.caCertificates
          (pkgs.python311.withPackages (ps: [ ps.google-cloud-kms ps.google-auth ps.gcsfs ps.duckdb ps.pycryptodome ]))
          (pkgs.runCommand "app.py" {} ''
            mkdir -p $out/app
            cp ${./decrypt_dek.py} $out/app/app.py
            export SSL_CERT_FILE=/etc/ssl/certs/ca-bundle.crt
          '')
        ];
        pathsToLink = [ "/etc/ssl/certs" "/bin" "/app" ];
      };
      config = {
        WorkingDir = "/app";
        Cmd = [ "python" "app.py" ];
      };
    };

  };
}
